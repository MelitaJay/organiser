from django.contrib import admin
from .models import MediaFile, Playlist, Category, PlaylistMediaFile

'''
    Registering Models so they appear in Django Admin
'''

class PlaylistMediaFileInline(admin.TabularInline):
    model = PlaylistMediaFile
    extra = 2

class CategoryInline(admin.TabularInline):
    model = MediaFile.categories.through
    extra = 2


class MediaFileAdmin(admin.ModelAdmin):
    model = MediaFile
    inlines = (PlaylistMediaFileInline, CategoryInline,)

    list_display = ['name', 'thumbnail', 'comment']

admin.site.register(MediaFile, MediaFileAdmin)


class CategoryAdmin(admin.ModelAdmin):
    model = Category

    list_display = ['name']

admin.site.register(Category, CategoryAdmin)


class PlaylistAdmin(admin.ModelAdmin):
    model = Playlist
    inlines = (PlaylistMediaFileInline,)

    list_display = ['name']

admin.site.register(Playlist, PlaylistAdmin)
