import os
from django.db import models

'''
    Models for MediaFile, Category, Playlist and PlaylistMediaFile
    PlaylistMediaFile is a through table for MediaFile and Playlist
    The ordinal field would be used to allow different ordering for each MediaFile per Playlist (Not implemented yet)
'''

class MediaFile(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to="file", null=True)
    thumbnail = models.ImageField(upload_to="thumbnail")
    comment = models.TextField()

    def __str__(self):
        return self.name

    # Filename and Filetype are shortcut methods 
    def filename(self):
        return os.path.splitext(self.file.name)[0].strip("file/")

    def filetype(self):
        return os.path.splitext(self.file.name)[1].strip(".")

    def to_json(self):

        return {
            "id": self.id,
            "name": self.name,
            "file": self.filename(),
            "thumbnail": self.thumbnail.url,
            "comment": self.comment,
        }

class Category(models.Model):
    name = models.CharField(max_length=255)
    files = models.ManyToManyField(MediaFile, related_name="categories")

    def __str__(self):
        return self.name

    def to_json(self):

        return {
            "id": self.id,
            "name": self.name,
            "files": [file.id for file in self.files.all()],
        }


class Playlist(models.Model):
    name = models.CharField(max_length=255)
    files = models.ManyToManyField(MediaFile, through="PlaylistMediaFile")

    def __str__(self):
        return self.name

    def to_json(self):

        return {
            "id": self.id,
            "name": self.name,
            "files": [file.id for file in self.files.all()],
        }

    def to_json_full(self):

        return {
            "id": self.id,
            "name": self.name,
            "files": [file.to_json() for file in self.files.all()],
        }


class PlaylistMediaFile(models.Model):
    mediaFile = models.ForeignKey(MediaFile, on_delete=models.CASCADE)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    ordinal = models.IntegerField(null=True)
    
    def __str__(self):
        return self.mediaFile.name
