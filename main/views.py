from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import JsonResponse

from .models import MediaFile, Category, Playlist

import json

class Home(View):

    # Get homepage
    def get(self, request):

        files = MediaFile.objects.all()

        ctx = {
            'files': files
        }

        return render(request, 'home.html', context=ctx)


class Search(View):

    def get(self, request, **kwargs):

        search_query = request.GET.get("q", "")
        files = MediaFile.objects.filter(name__contains=search_query)

        ctx = {
            'files': files
        }

        return render(request, 'home.html', context=ctx)

        
class JSON(View):

    # Downloading the JSON file
    def get(self, request):

        data = {}

        # Get all the media files
        mediafiles = MediaFile.objects.all()
        data["mediafiles"] = [file.to_json() for file in mediafiles]

        # Get all the Categories:
        categories = Category.objects.all()
        data["categories"] = [category.to_json() for category in categories]

        # Get all the Playlists:
        playlists = Playlist.objects.all()
        data["playlists"] = [playlist.to_json() for playlist in playlists]

        with open("data.json", "w") as out:
            print(json.dumps(data))
            out.write(json.dumps(data))

        return redirect('main:home')

    # Uploading the JSON file
    def post(self, request):

        # Get and process the json data
        data = request.FILES.get("upload").file.read().decode('utf8')
        data = json.loads(data)

        mediafile_ids = []
        # Update Mediafiles: name and comment
        for m in data.get("mediafiles", ""):
            mediafile_ids.append(m.get("id", ""))
            file = MediaFile.objects.filter(id=m.get("id", "")).first()
            # Can only update Media Files, not create them
            if file:
                file.name = m.get("name", "")
                file.comment = m.get("comment", "")
                file.save()

        # Delete mediafiles that aren't in the json
        MediaFile.objects.exclude(id__in=mediafile_ids).delete()
        
        category_ids = []
        # Update Categories: name and MediaFiles
        for c in data.get("categories", ""):
            category_ids.append(c.get("id", ""))
            cat = Category.objects.filter(id=c.get("id", "")).first()
            # If the Catgory exists, update it
            if cat:
                cat.name = c.get("name", "")
                # Reset all the relationships with MediaFiles
                cat.files.clear()
            # Else, create it
            else:
                cat = Category.objects.create(name=c.get("name", ""))
                category_ids.append(cat.id)
            files = MediaFile.objects.filter(id__in=c.get("files", ""))
            cat.files.add(*files)
            cat.save()
            
        # Delete categories that aren't in the json
        Category.objects.exclude(id__in=category_ids).delete()

        playlist_ids = [] 
        # Update Playlists: name and MediaFiles
        for p in data.get("playlists", ""):
            playlist_ids.append(p.get("id", ""))
            playlist = Playlist.objects.filter(id=p.get("id", "")).first()
            # If the Playlist exists, update it
            if playlist:
                playlist.name = p["name"]
             # Reset all the relationships with MediaFiles
                playlist.files.clear()
            # Else, create it
            else:
                playlist = Playlist.objects.create(name=p.get("name", ""))
                playlist_ids.append(playlist.id)
            files = MediaFile.objects.filter(id__in=p.get("files", ""))
            playlist.files.add(*files)
            playlist.save()
        # Delete playlists that aren't in the json
        Playlist.objects.exclude(id__in=playlist_ids).delete()

        return JsonResponse({'success':True})

        ctx = {
            'files': files
        }

        return render(request, 'home.html', context=ctx)

class MediaFileView(View):

    # Getting the Media File page
    def get(self, request, file_id, **kwargs):

        file = MediaFile.objects.get(id=file_id)
        categories = Category.objects.all()

        ctx = {
            'file': file,
            'categories': categories,
        }

        return render(request, 'file_view.html', context=ctx)

    # Updating the Media File
    def post(self, request, file_id, **kwargs):

        file = MediaFile.objects.get(id=file_id)
        categories = Category.objects.all()

        # Update name and comment
        file.name = request.POST["name"]
        file.comment = request.POST["comment"]

        # Go through the Category checkboxes and update
        for f, v in request.POST.items():
            if "checkbox" in f:
                cat_id = f.split("-")[1]
                category = Category.objects.get(id=cat_id)
                if v != "":
                    file.categories.add(category)
                else:
                    file.categories.remove(category)

        # If a thumbnail and file have been uploaded, update them
        if request.FILES.get("file", "") != "":
            file.file = request.FILES["file"]

        if request.FILES.get("thumbnail", "") != "":
            file.thumbnail = request.FILES["thumbnail"]

        file.save()

        ctx = {
            'file': file,
            'categories': categories,
            'saved': True
        }

        return render(request, 'file_view.html', ctx)


class MediaFileAdd(View):

    # Get the create new Media File page
    def get(self, request, **kwargs):

        categories = Category.objects.all()

        ctx = {
            'categories': categories
        }

        return render(request, 'file_add.html', context=ctx)

    # Create the new Media File with the data given  
    def post(self, request, **kwargs):

        file = MediaFile.objects.create(
            name=request.POST["name"],
            comment=request.POST["comment"],
        )

        # Go through the category checkboxes
        for f, v in request.POST.items():
            if "checkbox" in f:
                cat_name = f.split("-")[1]
                category = Category.objects.get(name=cat_name)
                if v != "":
                    file.categories.add(category)

        # Thumbnail and file
        if request.FILES.get("file", "") != "":
            file.file = request.FILES["file"]

        if request.FILES.get("thumbnail", "") != "":
            file.thumbnail = request.FILES["thumbnail"]

        file.save()

        ctx = {
            'file': file,
        }

        return redirect('main:file_view', file_id=file.id)


class MediaFileDelete(View):

    # Get the delete confirmation page
    def get(self, request, file_id, **kwargs):

        file = MediaFile.objects.get(id=file_id)

        ctx = {
            'file': file
        }

        return render(request, 'file_delete.html', context=ctx)

    # Delete the Media File
    def post(self, request, file_id, **kwargs):

        file = MediaFile.objects.filter(id=file_id).delete()

        return redirect('main:home')


class CategoriesView(View):

    # Get the Categories page
    def get(self, request, **kwargs):

        categories = Category.objects.all()

        ctx = {
            'categories': categories,
        }

        return render(request, 'categories_view.html', context=ctx)

    # Create a new Category
    def post(self, request, **kwargs):

        # If the Category doesn't already exist, create it
        new_name = request.POST["category"]
        category = Category.objects.filter(name=new_name)
        if not category.count():
            Category.objects.create(name=new_name)

        categories = Category.objects.all()

        ctx = {
            'categories': categories,
        }

        return render(request, 'categories_view.html', context=ctx)


class CategoryDelete(View):

    # Get the Category delete confirmation page
    def get(self, request, cat_id, **kwargs):

        category = Category.objects.get(id=cat_id)

        ctx = {
            'category': category,
        }

        return render(request, 'category_delete.html', context=ctx)

    # Delete the Category
    def post(self, request, cat_id, **kwargs):

        category = Category.objects.get(id=cat_id)
        category.delete()

        return redirect('main:categories_view')
        

class PlaylistsView(View):

    # Get the Playlists page
    def get(self, request, **kwargs):

        playlists = Playlist.objects.all()

        # Get the data for the playlists and store it in json for vue to access
        playlist_data = {}

        for playlist in playlists:
            files = playlist.files.all()
            playlist_data[playlist.name] = playlist.to_json_full()

        ctx = {
            "playlist_data": playlist_data,
        }

        return render(request, 'playlists_view.html', context=ctx)
    
    # Create a new Playlist
    def post(self, request, **kwargs):

        # If the playlist doesn't already exist, create it
        new_name = request.POST.get("playlist", "")
        playlist = Playlist.objects.filter(name=new_name)
        if not playlist.count():
            Playlist.objects.create(name=new_name)

        playlists = Playlist.objects.all()

        ctx = {
            'playlists': playlists,
        }

        return redirect('main:playlists_view')


class PlaylistView(View):

    # Get the page for a Playlist
    def get(self, request, playlist_id, **kwargs):

        playlist = Playlist.objects.get(id=playlist_id)
        mediaFiles = MediaFile.objects.all()

        ctx = {
            'playlist': playlist,
            'mediaFiles': mediaFiles
        }

        return render(request, 'playlist_view.html', context=ctx)

    def post(self, request, playlist_id, **kwargs):

        playlist = Playlist.objects.get(id=playlist_id)
        mediaFiles = MediaFile.objects.all()

        # Go through the Media File chckboxes and update
        for f, v in request.POST.items():
            if "checkbox" in f:
                file_id = f.split("-")[1]
                file = MediaFile.objects.get(id=file_id)
                print(30, v)
                if v != "":
                    playlist.files.add(file)
                else:
                    playlist.files.remove(file)

        ctx = {
            'playlist': playlist,
            'mediaFiles': mediaFiles,
            'saved': True
        }

        return render(request, 'playlist_view.html', context=ctx)


class PlaylistDelete(View):

    # Get Playlist delete confirmation page
    def get(self, request, playlist_id, **kwargs):

        playlist = Playlist.objects.get(id=playlist_id)

        ctx = {
            'playlist': playlist,
        }

        return render(request, 'playlist_delete.html', context=ctx)

    # Delete the Playlist
    def post(self, request, playlist_id, **kwargs):

        playlist = Playlist.objects.get(id=playlist_id)
        playlist.delete()

        return redirect('main:playlists_view')