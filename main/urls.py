from django.urls import path

from .views import (
    Home,
    JSON,
    Search,
    MediaFileView,
    MediaFileDelete,
    MediaFileAdd,
    CategoriesView,
    CategoryDelete,
    PlaylistsView,
    PlaylistDelete,
    PlaylistView
)

app_name = 'main'

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('search/', Search.as_view(), name='search'),
    path('json/', JSON.as_view(), name='json'),
    path('mediafile/<int:file_id>/delete', MediaFileDelete.as_view(), name='file_delete'),
    path('mediafile/<int:file_id>', MediaFileView.as_view(), name='file_view'),
    path('mediafile/add', MediaFileAdd.as_view(), name='file_add'),
    path('categories', CategoriesView.as_view(), name='categories_view'),
    path('categories/<int:cat_id>/delete', CategoryDelete.as_view(), name='category_delete'),
    path('playlists', PlaylistsView.as_view(), name='playlists_view'),
    path('playlist/<int:playlist_id>/delete', PlaylistDelete.as_view(), name='playlist_delete'),
    path('playlist/<int:playlist_id>', PlaylistView.as_view(), name='playlist_view'),

]