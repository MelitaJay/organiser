function validateForm() {

    // Need to check all the fields are valid before submitting
    var valid = true;

    var thumbnail = $("#thumbnail-input");
    var name = $("#name");
    var file = $("#file-input");

    if (thumbnail.val()) {
        thumbnail.parent().find(".required-text").css("visibility", "hidden")
    } else {
        thumbnail.parent().find(".required-text").css("visibility", "visible")
        valid = false;
    }

    if (name.val()) {
        name.parents().eq(1).find(".required-text").css("visibility", "hidden")
    } else {
        name.parents().eq(1).find(".required-text").css("visibility", "visible")
        valid = false;
    }

    if (file.val()) {
        file.parent().find(".required-text").css("visibility", "hidden")
    } else {
        file.parent().find(".required-text").css("visibility", "visible")
        valid = false;
    }
    
    if (valid) {
        $("form").submit();
    }
}