function validateForm() {

    // Need to check all the fields are valid before submitting
    var valid = true;

    var thumbnail = $("#thumbnail-input");
    var name = $("#name");
    var file = $("#file-input");

    if (name.val()) {
        name.parents().eq(1).find(".required-text").css("visibility", "hidden")
    } else {
        name.parents().eq(1).find(".required-text").css("visibility", "visible")
        valid = false;
    }

    if (valid) {
        $("form").submit();
    }

}
    
// Fade out for the saved text    
setTimeout(fadeout, 3000);
function fadeout() {
    $("#saved-text").css("opacity", "0");
}
