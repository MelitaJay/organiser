var playlist = new Vue({
    el: "#playlists",
    delimiters: ['[[', ']]'],
    data: {
        playlist: {},
    },
    methods: {
        changePlaylist: function (e) {
            var selected  = e.srcElement.innerText
            this.playlist = playlist_data[selected]
        }
    }
})