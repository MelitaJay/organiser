
$(document).ready(function() {

    $("#search-icon").on("click", function(e) {
        search();
    })

    $("#search-bar").on("keydown", function(e) {
        if(e.keyCode == 13) {
            search();
        }
    })

    function search() {
        $("#search-form").submit();
    }

    $("#json-upload-button").on("click", function() {
        $("#json-download").click();
    });

    $("#json-download").on("change", function(e) {
        var data = new FormData();
        file = e.target.files[0];
        data.append("upload", file);
        $.ajax({
            type: "POST",
            headers: { "X-CSRFToken": token },
            url: json_upload_url,
            data: data,
            contentType: false,
            processData: false,
        });
    });


    $("#upload-thumbnail").on("click", function() {
        $("#thumbnail-input").click();
    });

    $("#thumbnail-input").on("change", function(e) {
        image = e.target.files[0];

        var reader = new FileReader();
        reader.onload = (function(file) {
            return function(e) {
                $("#full-thumbnail").attr("src", e.target.result)
            }
        })(image);
        reader.readAsDataURL(image);

    });


    function humanFileSize(size) {
        var i = Math.floor( Math.log(size) / Math.log(1024) );
        return ( size / Math.pow(1024, i) ).toFixed(1) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    };

    $("#upload-file").on("click", function() {
        $("#file-input").click();
    });

    $("#file-input").on("change", function(e) {

        file = e.target.files[0];
        var filename = file.name.replace(/^.*[\\\/]/, '').split(".")
        $("#filename").text(filename[0])
        $("#filetype").text(filename[1])
        $("#filesize").text(humanFileSize(file.size).toUpperCase())

    });

    $("#save-button").on("click", function(e) {
        var checkboxes = $("#checkboxes input[type='checkbox']")
        checkboxes.each(function(e) {
            var id = $(checkboxes[e]).attr("id") + "-hidden"
            // If is ticked, don't need the hidden checkbox to be sent
            if ($(checkboxes[e]).is(":checked")) {
                $("#"+id).attr("disabled", "true")
            }
        });

        validateForm();
    })

});
